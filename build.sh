#!/usr/bin/env bash
set -e

function docker_tag_exists() {
  curl --silent -f -lSL https://index.docker.io/v1/repositories/$1/tags/$2 > /dev/null
}

function add_label() {
  sed -i "2iLABEL $1=\"$2\"" "$SLATE_DIR/Dockerfile"
}

RELEASE_TAG=$(curl --silent "https://api.github.com/repos/slatedocs/slate/releases/latest" |
            grep '"tag_name":' |
            sed -E 's/.*"([^"]+)".*/\1/')
VERSION=$(echo "$RELEASE_TAG" | cut -c "2-")

if docker_tag_exists joniator/slatedocs $RELEASE_TAG; then
  echo "Latest tag $RELEASE_TAG exists, exiting without building"
  exit 0
else
  SLATE_DIR="slate-$VERSION"

  wget "https://github.com/slatedocs/slate/archive/$RELEASE_TAG.tar.gz" -O slate.tar.gz
  tar -xzf slate.tar.gz

  add_label "maintainer" "Jonathan Boeckel <jonathanboeckel1996@gmail.com>"
  add_label "version" "$VERSION"

  DOCKER_LATEST_TAG="joniator/slatedocs:latest"
  DOCKER_VERSION_TAG="joniator/slatedocs:$RELEASE_TAG"
  docker build --pull -t "$DOCKER_LATEST_TAG" -t "$DOCKER_VERSION_TAG" "$SLATE_DIR"
  docker push "$DOCKER_LATEST_TAG"
  docker push "$DOCKER_VERSION_TAG"

  rm slate.tar.gz
  rm -rf "$SLATE_DIR"
fi
